# 基于CDH Hive的自定义身份信息认证

## 配置步骤:

### Step1: Web界面进入CM配置HDFS

> #### HDFS修改core-site.xml配置
>
> **搜索 core-site.xml**
>
> core-site.xml 的群集范围高级配置代码段（安全阀）

```shell
<property>
    <name>hadoop.proxyuser.hadoop.hosts</name>
    <value>*</value>
</property>
<property>
    <name>hadoop.proxyuser.hadoop.groups</name>
    <value>*</value>
</property>
```

图示: 

![image](images/hdfs1.png)

### Step2: Web界面进入CM配置YARN

> #### YARN (MR2 Included)修改core-site.xml配置
>
> **搜索 core-site.xml**
>
> core-site.xml 的 YARN 服务高级配置代码段（安全阀）添加

```shell
<property>
    <name>hadoop.proxyuser.hadoop.hosts</name>
    <value>*</value>
</property>
<property>
    <name>hadoop.proxyuser.hadoop.groups</name>
    <value>*</value>
</property>

<!-- "*" 表示可通过超级代理“xxx”操作hadoop的用户、用户组和主机 -->
```

图示:

![image](images/yarn1.png)

> 报错信息: 
>
> java.sql.SQLException: Could not open client transport with JDBC Uri: jdbc:hive2://localhost:10000/default: java.net.ConnectException: 拒绝连接

解决方式：在hadoop的配置文件core-site.xml增加如下配置，重启hdfs，其中“xxx”是连接beeline的用户，将“xxx”替换成自己的用户名即可。**最关键的是一定要重启hadoop，先stop-all.sh，再start-all.sh，否则不会生效的**！！那样就还是报错！

### Step3: Hive修改hive-site.xml配置

> **搜索 hive-site.xml**
>
> hive-site.xml 的 Hive 服务高级配置代码段（安全阀）
>
> **搜索 Hive 辅助 JAR 目录**
>
> 找到hive的辅助目录的路径, 如:
>
> -  **/usr/share/java/ 是hive的辅助目录**

```shell
<property>
    <name>hive.server2.authentication</name>
    <value>CUSTOM</value>
    <description>自定义HiveServer2类</description>
</property>
<property>
    <name>hive.server2.custom.authentication.class</name>
    <value>com.joy.hive.auth.CustomHiveServer2Auth</value>
    <description>自定义HiveServer2类的包名.类名</description>
</property>
<property>
    <name>hive.server2.custom.authentication.file</name>
    <value>/usr/share/java/hive-conf/hive.server2.users.conf</value>
    <description>自定义HiveServer2类的身份信息</description>
</property>
```

图示:

![image](images/hive1.png)

### Step4: Hive修改hive-site.xml配置

将打包好的jar重命名为HiveServer2Auth.jar, 放到 **Hive 辅助 JAR 目录 /usr/share/java/** 下

① 注: 将 HiveServer2Auth.jar 放到所有 hive 的节点上

```shell
# 将 HiveServer2Auth.jar 上传到服务器的/opt/data/目录下
# 将jar上传到 Hive辅助JAR 目录下
[joy@hadoop002 data]# cp /opt/data/HiveServer2Auth.jar /usr/share/java/
scp /usr/share/java/HiveServer2Auth.jar root@hadoop001:/usr/share/java/
scp /usr/share/java/HiveServer2Auth.jar root@hadoop002:/usr/share/java/
scp /usr/share/java/HiveServer2Auth.jar root@hadoop003:/usr/share/java/
```

### Step5: 在Hive/conf下新建 **hive.server2.users.conf** 文件, 用来存放用户名和密码

~~注: 将这个 hive.server2.users.conf 文件需要放到所有 HiveServer2 服务 节点上~~

~~进入CDH中的lib/hive目录下找到conf, 再将hive.server2.users.conf放到 /etc/hive/conf 目录下~~

> #### 注意: 每次进行修改hive-site.xml配置时, 重启hive后, 都会刷新/etc/hive/conf 下的所有文件, 导致每次更hive配置文件都需要需要重新配置hive.server2.users.conf文件, 为避免这种情况发生, 在hive的辅助目录下新建文件夹 hive-conf, 将hive.server2.users.conf 文件放到/usr/share/java/hive-conf路径下
>
> #### 最后将hive.server2.users.conf文件 分发到所有hive节点上

### Step6: 登录测试

#### beeline连接hive

```SQL
beeline -u jdbc:hive2://hadoop002:10000 -n hive -p hive

beeline -u jdbc:hive2://hadoop002:10000 -n hive -p hive -e "select * from db.table_name"
```

### Step7: 权限分配, CDH上是Sentry控制权限, 具体内容百度搜索......

各组件版本信息:

CDH5.16.2

Hive 1.1.0-cdh5.16.2